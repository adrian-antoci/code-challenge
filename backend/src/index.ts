import express, { Express, Request, Response } from 'express';
import cors from 'cors';
import { AutocompleteService, UploadService } from './service/service';
import getRawBody from 'raw-body';
import * as http from 'http';

/**
 * Endpoints for the code challenge
 * 
 * Future improvements:
 * - use a different DB for autocomplete queries like ElasticSearch
 * - get rid of the ugly relative paths e.g. from './service/service';
 * - make sure we are safe of SQL injection attacks
 * - make sure we can do case insensitive queries
 * - better type checks
 * - add proper CORS and JWT security checks
 * - add dotenv lib
 * - automate deployment
 * - prepush linting
 * - custom domain
 */

const app: Express = express();
app.use(express.json({
    limit: '5mb',
}));
app.use(cors()); // FIXME add the frontend domain

const port = 8081;

/**
 * Can be used to do a health check.
 */
app.get('/', (req: Request, res: Response) => {
    res.send('Ready');
});

/**
 * API to populate DB values
 * Used only internally.
 */
app.post('/seed', express.raw(), async (req: Request, res: Response) => {
    const service = new UploadService();
    const body = await getRawBody(req);
    const response = await service.serve(body.toString('utf8'));
    res.status(response.statusCode).send(response.data);
});

/**
 * API to handle autocomplete requests from client.
 */
app.get('/locations', async (req: Request, res: Response) => {
    const service = new AutocompleteService();
    const response = await service.serve(req.query.q as string); // FIXME better type check
    res.status(response.statusCode).send(response.data);
});

app.listen(port, () => {
    console.log(`⚡️[server]: Server is running at http://localhost:${port}`);
});
