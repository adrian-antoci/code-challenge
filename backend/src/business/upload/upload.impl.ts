import { SqLiteDatabase } from "../database/database";

export class UploadServiceImpl {

    /**
     * 
     * @param body TSV utf8 body
     * @returns true if successful
     */
    async populateDB(body: string): Promise<boolean> {
        const rows = this.parseTSV(body);
        const db = new SqLiteDatabase();
        await db.populate(rows);
        return true;
    }

    /**
     *
     * @param body TSV body as string
     * @returns An array of TSVValue type
     */
    private parseTSV(body: string) {
        const result: PlaceDTO[] = [];

        const lines = body.split('\n');
        for (let i = 1; i < lines.length; i++) {
            const values = lines[i].split('\t');
            result.push({ id: values[0], name: values[1] });
        }
        return result;
    }
}

/**
 * Interface used to parse data from TSVFile
 */
export interface PlaceDTO {
    id: string;
    name: string;
}