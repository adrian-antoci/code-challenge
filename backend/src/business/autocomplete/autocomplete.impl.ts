import { SqLiteDatabase } from "../database/database";

export class AutocompleteImpl {

    readonly queryResultLimit = 10;
    readonly queryMinChars = 2;

    /**
     * Will return a list of results based on query string
     * 
     * // FIXME make sure we cannot SQL inject
     * @param queryString 
     * @returns List of results
     */
    async findByText(queryString: string | undefined): Promise<string[]> {
        if (queryString === undefined || queryString.length <= this.queryMinChars) {
            return [];
        }
        const db = new SqLiteDatabase(); // FIXME this should be injected as dependency so we can mock it

        const results = await db.fetch(queryString, this.queryResultLimit);
        return results.map(it => it.name);
    }

}