

var sqlite3 = require("sqlite3").verbose();

import { PlaceDTO } from "../upload/upload.impl";

export class SqLiteDatabase {

    /**
     * Will populate the 'place' table with TSVValues
     * @param rows
     */
    async populate(rows: PlaceDTO[]) {

        const db = this.dbInstance();

        db.serialize(() => {
            db.run("CREATE TABLE IF NOT EXISTS place (name TEXT)");

            const stmt = db.prepare("INSERT INTO place VALUES (?)");

            rows.forEach(item => {
                stmt.run(item.name);
            });
            stmt.finalize();
        });

        db.close();
    }

    /**
     * // TODO use cursor to get more results
     *
     * @param queryString E.g 'Oxford'
     * @param limit Max number of results
     * @returns List of PlaceDTO
     */
    async fetch(queryString: string, limit: number): Promise<PlaceDTO[]> {
        return new Promise((resolve, reject) => {
            const db = this.dbInstance();

            db.all(`SELECT DISTINCT rowid AS id, name FROM place WHERE name LIKE '${queryString}%' LIMIT ${limit}`, (err: any, data: any) => {
                const results: PlaceDTO[] = [];
                if (data) {
                    console.log(data);
                    data.forEach((element: { id: any; name: any; }) => {
                        results.push({ id: element.id, name: element.name });
                    });
                }
                // always resolve for now
                // TODO reject if error
                resolve(results);
            });
            db.close();
        });
    }
    private dbInstance() {
        return new sqlite3.Database('/db/main.db');
    }
}