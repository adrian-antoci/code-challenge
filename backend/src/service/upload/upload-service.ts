import { UploadServiceImpl } from "../../business/business";
import { HttpResponse } from "../../utils/utils";

export class UploadService {
    /**
     * Serves the actual request
     *
     * @param body
     * @returns Always an HttpResponse
     */
    async serve(body: any): Promise<HttpResponse> {
        const logic = new UploadServiceImpl();
        const result = await logic.populateDB(body);
        return new HttpResponse({ statusCode: result ? 200 : 503, data: 'DONE' });
    }
}