import { AutocompleteImpl } from "../../business/business";
import { HttpResponse } from "../../utils/utils";

export class AutocompleteService {
    /**
     * Serves the actual request
     *
     * @param queryString
     * @returns Always an HttpResponse
     */
    async serve(queryString: string): Promise<HttpResponse> {
        try {
            const logic = new AutocompleteImpl();
            const results = await logic.findByText(queryString);

            return new HttpResponse({ statusCode: 200, data: results });
        } catch (ex) {
            // TODO capture exception, send to CloudWatch or similar
            // For now it will return 500
        }
        return new HttpResponse();
    }
}