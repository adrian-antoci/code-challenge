import { AutocompleteService } from "./autocomplete/autocomplete.service";
import { UploadService } from "./upload/upload-service";

/// Export the implementation classes
export { AutocompleteService, UploadService };