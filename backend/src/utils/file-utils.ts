import path from "path";

/**
 * Some file related util functions.
 */
export class FileUtils {

    /**
     * 
     * @param fileName
     * @returns The actual extension e.g. .pdf
     */
    static getExt(fileName: string): string {
        let ext = path;
        return ext.extname(fileName);
    }

    /**
     * 
     * @param value 
     * @returns {true} if value is null of empty
     */
    static isNullOrEmpty(value: string | null): boolean {
        return value == null || value.length === 0;
    }

    /**
     * 
     * @param ext
     * @returns {true} if value if the specific extension
     */
    static isTdFile(ext: string): boolean {
        return ext.endsWith(".pdf");
    }
}