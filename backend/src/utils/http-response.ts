export class HttpResponse {
    statusCode = 500;
    data = {};
    error = "";

    public constructor(init?: Partial<HttpResponse>) {
        Object.assign(this, init);
    }
}