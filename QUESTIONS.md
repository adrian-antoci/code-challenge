# Questions

Q1: Explain the output of the following code and why

```js
    setTimeout(function() {
      console.log("1");
    }, 100);
    console.log("2");
```
A1: The output is a console message "2" and then "1" 100ms later because of the 'timeout' function.


Q2: Explain the output of the following code and why

```js
    function foo(d) {
      if(d < 10) {
        foo(d+1);
      }
      console.log(d);
    }
    foo(0);
```

A2: The output is a sequence of console logs from 10 to 0. The 'foo()' function is called recursively if the input is less than 10.

Q3: If nothing is provided to `foo` we want the default response to be `5`. Explain the potential issue with the following code:

```js
    function foo(d) {
      d = d || 5;
      console.log(d);
    }
```

A3: The function seems to work as expected with no obvious issues.


Q4: Explain the output of the following code and why

```js
    function foo(a) {
      return function(b) {
        return a + b;
      }
    }
    var bar = foo(1);
    console.log(bar(2))
```
A4: The output of the function is 3. The function 'foo()' returns another function that adds a and b together. The variable 'bar' will contain the actual function and value 'a' and console.log call to 'bar()' function is providing value 'b'.

Q5: Explain how the following function would be used

```js
    function double(a, done) {
      setTimeout(function() {
        done(a * 2);
      }, 100);
    }
```

Q5: The 'double()' function accepts two parameters: one is a value and the other is a callback function. This is how to use it:
double(2,(res) => {
  console.log(res); // in 100ms, result returned is 4
})