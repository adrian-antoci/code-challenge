import React, { Component, Fragment } from "react";

/**
 * The Autocomplete component responsable for displaying suggestions while the user is typing.
 *
 */
export default class AutocompleteComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            filteredSuggestions: [],
            showSuggestions: false,
            abortController: null,
            userInput: "",
            baseAPI: "https://heygo-backend-6ffvy6tysq-lz.a.run.app", // FIXME this should be a constant and NOT HERE
        };
    }

    /**
     * Called when user keyboard event is triggered
     *
     * @param {*} e 
     */
    onKeyboardChange = e => {
        const userInput = e.currentTarget.value;

        this.getData(userInput);
    };

    /**
     * On suggestion selected, remove the result list
     * Will update state.
     *
     * @param {*} e 
     */
    onSuggestionSelected = e => {
        this.setState({
            filteredSuggestions: [],
            showSuggestions: false,
            userInput: e.currentTarget.innerText
        });
    };

    /**
     *
     * @param {*} e 
     */
    onKeyDown = e => {
        const { filteredSuggestions } = this.state;

        // On Enter key
        if (e.keyCode === 13) {
            this.setState({
                showSuggestions: false,
                userInput: filteredSuggestions
            });
        }
    };

    /**
     * Will fetch results from API, cancel previous requests if any and 
     * update state.
     * 
     * @param {*} queryString 
     */
    getData = (queryString) => {
        if (this.state.abortController !== null) {
            this.state.abortController.abort();
        }
        const controller = new AbortController();
        const signal = controller.signal;

        this.setState({
            abortController: controller
        });

        /// The api logic usually sits in a different module
        /// For simplicity everything is written here 
        fetch(this.state.baseAPI + "/locations?q=" + queryString, {
            signal,
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json"
            }
        })
            .then((response) => {
                return response.json();
            })
            .then((response) => {
                if (!response) {
                    return;
                }
                this.setState({
                    filteredSuggestions: response,
                    showSuggestions: true,
                    userInput: queryString
                });
            });
    };

    /**
     * React render function. 
     *
     * @returns 
     */
    render() {
        const {
            onKeyboardChange,
            state: {
                userInput
            }
        } = this;

        let htmlComponent = this.buildResults();

        return (
            <Fragment>
                <div className="container">
                    <input
                        type="text"
                        placeholder="Type place name..."
                        onChange={onKeyboardChange}
                        value={userInput}
                    />
                    {htmlComponent}
                </div>
            </Fragment>
        );
    }

    /**
     *
     * @returns HTML for the Render function based on state
     */
    buildResults() {
        const {
            onSuggestionSelected,
            state: {
                filteredSuggestions,
                showSuggestions,
                userInput
            }
        } = this;

        // Either we display a list of text saying there are no suggestions
        if (showSuggestions && userInput) {
            if (filteredSuggestions.length) {
                return (
                    <ul className="suggestions">
                        {filteredSuggestions.map((suggestion, index) => {

                            return (
                                <li key={suggestion} onClick={onSuggestionSelected}>
                                    {suggestion}
                                </li>
                            );
                        })}
                    </ul>
                );
            } else {
                return (
                    <div className="no-suggestions">
                        <em>No suggestions so far</em>
                    </div>
                );
            }
        }
    }
}
