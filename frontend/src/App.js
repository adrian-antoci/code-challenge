
import './App.css';
import AutocompleteComponent from './components/autocomplete';

function App() {
  return (
    <div className="body">
      <div>
        <h1>
          Heygo Autocomplete
        </h1>
      </div>
      <br />
      <div>
        <AutocompleteComponent
        />
      </div>
    </div>
  );
}

export default App;
